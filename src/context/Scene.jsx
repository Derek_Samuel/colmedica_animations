import React from "react";
import * as THREE from "three";

export const Scene = React.createContext();

export const SceneProvider = ({ children }) => {
  React.useEffect(() => {
    let canvas = document.querySelector("#canvas");
    const scene = new THREE.Scene();
    const fov = 75; // Grados
    const aspect = window.innerWidth / window.innerHeight; // Relación de aspecto
    const near = 0.1; // Si el objeto está más cerca que esta distancia no se renderiza
    const far = 5; // Si el objeto está más lejos que esta distancia no se renderiza

    const camera = new THREE.PerspectiveCamera(fov, aspect, near, far);

    camera.position.z = 2;

    var renderer = new THREE.WebGLRenderer();
    renderer.setSize( window.innerWidth, window.innerHeight );
    canvas.appendChild(renderer.domElement);

    const boxWidth = 1;
    const boxHeight = 1;
    const boxDepth = 1;

    const geometry = new THREE.BoxGeometry(boxWidth, boxHeight, boxDepth);
    const material = new THREE.MeshPhongMaterial({ color: "red" });

    const cube = new THREE.Mesh(geometry, material);
    scene.add(cube);
  }, []);

  return (
    <div>
      <div style={{ width: "100vw", height: "100vh", }} id="canvas"></div>
      { children }
    </div>
  );
};
