import React, { useEffect, useRef } from "react";
import * as THREE from "three";

export default function App({ children }) {
  const ref = useRef(null);
  let count = 0;
  let ids = [];
  let idsCaminarCall = [];
  let turns = ["A1", "A2", "A0", "A5", "A0"];
  const scene = new THREE.Scene();
  const camera = new THREE.PerspectiveCamera(75, window.innerWidth / window.innerHeight, 0.1, 1000);
  const renderer = new THREE.WebGLRenderer();
  renderer.setSize(window.innerWidth, window.innerHeight);

  function createPerson(caminarCall, number, z, x) {
    const person = new THREE.Group();
    //-------------------HEAD---------------------
    const geometrycabll = new THREE.SphereGeometry( 0.35, 10, 10 );
    const materialcabll = new THREE.MeshPhongMaterial( { color: 0x5D2C03 } );
    const cabll = new THREE.Mesh( geometrycabll, materialcabll );

    const geometryNose = new THREE.CylinderGeometry(0.03, 0.04, 0.1, 60);
    const materialNose = new THREE.MeshPhongMaterial( { color: 0xF9E7AD } );
    const nose = new THREE.Mesh( geometryNose, materialNose );

    const geometrycabll2 = new THREE.SphereGeometry( 0.18, 10, 10 );
    const materialcabll2 = new THREE.MeshPhongMaterial( { color: 0x5D2C03 } );
    const cabll2 = new THREE.Mesh( geometrycabll2, materialcabll2 );

    const geometrycabll3 = new THREE.SphereGeometry( 0.15, 10, 10 );
    const materialcabll3 = new THREE.MeshPhongMaterial( { color: 0x5D2C03 } );
    const cabll3 = new THREE.Mesh( geometrycabll3, materialcabll3 )

    const geometrycabll4 = new THREE.SphereGeometry( 0.2, 10, 10 );
    const materialcabll4 = new THREE.MeshPhongMaterial( { color: 0x5D2C03 } );
    const cabll4 = new THREE.Mesh( geometrycabll4, materialcabll4 )

    const geometrycabll5 = new THREE.SphereGeometry( 0.2, 10, 10 );
    const materialcabll5 = new THREE.MeshPhongMaterial( { color: 0x5D2C03 } );
    const cabll5 = new THREE.Mesh( geometrycabll5, materialcabll5 )

    const geometryHead = new THREE.CylinderGeometry(0.2, 0.1, 0.4, 60);
    const materialHead = new THREE.MeshPhongMaterial( { color: 0xF9E7AD } );
    const circle1 = new THREE.Mesh( geometryHead, materialHead );

    const geometryCircle2 = new THREE.SphereGeometry( 0.28, 10, 10 );
    const materialCircle2 = new THREE.MeshPhongMaterial( { color: 0xF9E7AD } );
    const circle2 = new THREE.Mesh( geometryCircle2, materialCircle2 );
    
    const yee1 = new THREE.CylinderGeometry(0.05, 0.06, 0.03, 45);
    const eye1Material = new THREE.MeshPhongMaterial( { color: 0xFFFFFF } );
    const eye1 = new THREE.Mesh( yee1, eye1Material );

    const yee2 = new THREE.CylinderGeometry(0.05, 0.06, 0.03, 45);
    const eye2Material = new THREE.MeshPhongMaterial( { color: 0xFFFFFF } );
    const eye2 = new THREE.Mesh( yee2, eye2Material );

    const ceja1s = new THREE.CylinderGeometry(0.05, 0.06, 0.02, 45);
    const ceja1Material = new THREE.MeshPhongMaterial( { color: 0x5D2C03 } );
    const ceja1 = new THREE.Mesh( ceja1s, ceja1Material );

    const ceja2s = new THREE.CylinderGeometry(0.05, 0.06, 0.02, 45);
    const ceja2Material = new THREE.MeshPhongMaterial( { color: 0x5D2C03 } );
    const ceja2 = new THREE.Mesh( ceja2s, ceja2Material );

    const mouthGeometry = new THREE.CylinderGeometry(0.05, 0.06, 0.03, 45);
    const mouthMaterial = new THREE.MeshPhongMaterial( { color: 0xFF6957 } );
    const mouth = new THREE.Mesh( mouthGeometry, mouthMaterial );

    const point1Geometry = new THREE.CircleGeometry(0.02, 10, 1);
    const point1Material = new THREE.MeshPhongMaterial( { color: 0x0072AF, } );
    const point1 = new THREE.Mesh( point1Geometry, point1Material );

    const point2Geometry = new THREE.CircleGeometry(0.02, 10, 1);
    const point2Material = new THREE.MeshPhongMaterial( { color: 0x0072AF, } );
    const point2 = new THREE.Mesh( point2Geometry, point2Material );

    circle2.renderOrder = 6;
    circle2.material.depthTest = false;
    eye1.renderOrder = 7;
    eye1.material.depthTest = false;
    eye2.renderOrder = 7;
    eye2.material.depthTest = false;
    circle1.renderOrder = 5;
    circle1.material.depthTest = false;
    ceja1.renderOrder = 8;
    ceja1.material.depthTest = false;
    ceja2.renderOrder = 8;
    ceja2.material.depthTest = false;
    cabll.renderOrder = 2;
    cabll.material.depthTest = false;
    cabll2.renderOrder = 8;
    cabll2.material.depthTest = false;
    cabll3.renderOrder = 2;
    cabll3.material.depthTest = false;
    cabll4.renderOrder = 2;
    cabll4.material.depthTest = false;
    cabll5.renderOrder = 8;
    cabll5.material.depthTest = false;
    nose.renderOrder = 8;
    nose.material.depthTest = false;
    mouth.renderOrder = 8;
    mouth.material.depthTest = false;
    point1.renderOrder = 9;
    point1.material.depthTest = false;
    point2.renderOrder = 9;
    point2.material.depthTest = false;

    eye1.position.y = 0.15;
    eye1.position.x = -0.08;
    eye2.position.y = 0.15;
    eye2.position.x = 0.08;
    circle2.position.y = 0.1;
    ceja1.position.y = 0.21;
    ceja1.position.x = -0.08;
    ceja2.position.y = 0.21;
    ceja2.position.x = 0.08;
    cabll.position.y = 0.08;
    cabll2.position.y = 0.35;
    cabll2.position.x = 0.3;
    cabll3.position.y = 0;
    cabll3.position.x = 0.35;
    cabll4.position.y = 0.08;
    cabll4.position.x = -0.3;
    cabll5.position.y = 0.45;
    nose.position.y = 0.08;
    mouth.position.y = -0.02;
    point1.position.y = 0.15;
    point1.position.x = -0.09;
    point2.position.y = 0.15;
    point2.position.x = 0.075;

    person.add( point1 );
    person.add( point2 );
    person.add( mouth );
    person.add( nose );
    person.add( cabll5 );
    person.add( cabll4 );
    person.add( cabll3 );
    person.add( cabll2 );
    person.add( cabll );
    person.add( ceja1 );
    person.add( ceja2 );
    person.add( eye1 );
    person.add( eye2 );
    person.add( circle1 );
    person.add( circle2 );

    //-------------------BODY---------------------

    new THREE.TextureLoader().load( "/assets/images/ropa/pink.jpg", (texture) => {
      const geometrytronco = new THREE.CylinderGeometry(0.2, 0.6, 0.85, 60);
      const materialtronco = new THREE.MeshPhongMaterial( { map: texture, } );
      const tronco = new THREE.Mesh( geometrytronco, materialtronco );

      tronco.position.y = -0.64;
      tronco.renderOrder = 4;
      person.add( tronco );
      renderer.render(scene, camera);
    } );

    const antearmGeometry = new THREE.CylinderGeometry(0.2, 0.2, 0.1, 45);
    const materialantearm = new THREE.MeshStandardMaterial( { color: 0xFFFFFF } );
    const antearm1 = new THREE.Mesh( antearmGeometry, materialantearm );

    antearm1.position.x = -0.25;
    antearm1.position.y = -0.6;
    antearm1.renderOrder = 9;
    antearm1.material.depthTest = false;
    // antearm1.rotation.x = -;
    person.add( antearm1 );

    const antearm2Geometry = new THREE.CylinderGeometry(0.2, 0.2, 0.1, 45);
    const materialantearm2 = new THREE.MeshStandardMaterial( { color: 0xFFFFFF } );
    const antearm21 = new THREE.Mesh( antearm2Geometry, materialantearm2 );

    antearm21.position.x = 0.3;
    antearm21.position.y = -0.6;
    antearm21.renderOrder = 9;
    antearm21.material.depthTest = false;
    // antearm21.rotation.x = -;
    person.add( antearm21 );

    const musloGeometry = new THREE.CylinderGeometry(0.2, 0.1, 0.8, 60);
    const materialmuslo = new THREE.MeshStandardMaterial( { color: 0xFFFFFF } );
    const muslo1 = new THREE.Mesh( musloGeometry, materialmuslo );

    muslo1.position.x = 0.2;
    muslo1.position.y = -1;
    muslo1.renderOrder = 3;
    muslo1.material.depthTest = false;
    // muslo1.rotation.x = -;
    person.add( muslo1 );

    const muslo2Geometry = new THREE.CylinderGeometry(0.2, 0.1, 0.8, 60);
    const materialmuslo2 = new THREE.MeshStandardMaterial( { color: 0xFFFFFF } );
    const muslo21 = new THREE.Mesh( muslo2Geometry, materialmuslo2 );

    muslo21.position.x = -0.2;
    muslo21.position.y = -1;
    muslo21.renderOrder = 3;
    muslo21.material.depthTest = false;
    // muslo21.rotation.x = -;
    person.add( muslo21 );

    const Shoe1Geometry = new THREE.BoxGeometry(0.2, 0.1, 0.4,);
    const materialShoe1 = new THREE.MeshStandardMaterial( { color: 0x0000 } );
    const Shoe11 = new THREE.Mesh( Shoe1Geometry, materialShoe1 );

    Shoe11.position.x = -0.2;
    Shoe11.position.y = -1.45;
    Shoe11.renderOrder = 2;
    Shoe11.material.depthTest = false;
    // Shoe11.rotation.x = -;
    person.add( Shoe11 );

    const Shoe2Geometry = new THREE.BoxGeometry(0.2, 0.1, 0.4,);
    const materialShoe2 = new THREE.MeshStandardMaterial( { color: 0x0000 } );
    const Shoe21 = new THREE.Mesh( Shoe2Geometry, materialShoe2 );

    Shoe21.position.x = 0.2;
    Shoe21.position.y = -1.45;
    Shoe21.renderOrder = 2;
    Shoe21.material.depthTest = false;
    // Shoe21.rotation.x = -;
    person.add( Shoe21 );

    // const handd = new THREE.CylinderGeometry(0.1, 0.06, 0.03, 45);
    // const handMaterial = new THREE.MeshPhongMaterial( { color: 0xFFFFFF } );
    // const hand = new THREE.Mesh( handd, handMaterial );

    // hand.renderOrder = 9;
    // hand.material.depthTest = false;

    // person.add( hand );

    const arm2Geometry = new THREE.TorusGeometry( 0.3, 0.1, 10, 5 );
    const materialArm2 = new THREE.MeshPhongMaterial( { color: 0xF9E7AD } );
    const arm2 = new THREE.Mesh( arm2Geometry, materialArm2 );

    arm2.position.x = 0.12;
    arm2.position.y = -0.6;
    arm2.renderOrder = 3;
    arm2.material.depthTest = false;
    // arm2.rotation.x = -;
    person.add( arm2 );

    const armGeometry = new THREE.TorusGeometry( 0.3, 0.1, 10, 5 );
    const materialArm = new THREE.MeshPhongMaterial( { color: 0xF9E7AD } );
    const arm1 = new THREE.Mesh( armGeometry, materialArm );

    arm1.position.x = -0.2;
    arm1.position.y = -0.6;
    arm1.rotation.y = -2;
    arm1.renderOrder = 3;
    arm1.material.depthTest = false;
    // arm1.rotation.x = -;
    person.add( arm1 );

    person.position.y = 1.5;

    scene.add( person );
    if (caminarCall) {
      idsCaminarCall.push(person);
    } else
      ids.push(person);
    renderer.render(scene, camera);
    if (caminarCall) {
      if (z && x) {
        console.log(x, z);
        person.position.x = x;
        person.position.z = z;
        renderer.render(scene, camera);
      }
    } else {
      const inter = setInterval(() => {
        if (count == 4) {
          const timeoutGeneral = setTimeout(() => {
            person.position.x = -0.8;
            const timer = setTimeout(() => {
              person.position.x = -1.9;
              scene.remove(ids[0]);
              ids.shift();
              turns.shift();
              renderer.render(scene, camera);
              clearTimeout(timer);
              clearTimeout(timeoutGeneral);
              clearInterval(inter);
            }, 2000);
          }, 4000);
        }
      }, 1000);
      const caminar = (direction) => {
        if (direction === "bottom") {
          count += 1;
          console.log(direction);
          muslo21.rotation.x = 0.5;
          Shoe11.rotation.x = 0.5;
          person.rotation.x = 0.2;
          muslo1.rotation.x = 0;
          Shoe21.rotation.x = 0;
          person.position.y += -0.2;
          const timeout = setInterval(() => {
            person.rotation.x = 0;
            muslo21.rotation.x = 0;
            Shoe11.rotation.x = 0;
            muslo1.rotation.x = 0.5;
            Shoe21.rotation.x = 0.5;
            person.position.y += -0.2;
            person.add(muslo21);
            person.add(Shoe11);
            renderer.render(scene, camera);
            clearInterval(timeout);
          }, 1000);
          if (count == 2) person.position.z = 0.25;
          if (count == 3) person.position.z = 0.5;
          if (count == 4) {
            person.position.z = 1.5;
            person.position.y = -0.6;
            person.position.x = -0.3;
            // person.rotation.y = -15;
            // eye1.position.x = 0.05;
            point1.position.y = 0.15;
            point1.position.x = -0.07;
            point2.position.y = 0.15;
            point2.position.x = 0.09;
          }
          person.add(muslo21);
          person.add(Shoe11);
          renderer.render(scene, camera);
        }
        else {}
      }
      // if (count === 4) return;
      const interval = setInterval(() => {
        if (count === 4) {
          clearInterval(interval);
          if (turns.length !== 1) {
            count = 0;
            if (turns.length == 3) {
              scene.remove(idsCaminarCall[1]);
              idsCaminarCall.pop();
            }
            if (turns.length == 2) {
              console.log(idsCaminarCall);
              scene.remove(idsCaminarCall[0]);
              idsCaminarCall.pop();
              renderer.render(scene, camera);
            }
            renderer.render(scene, camera);
            createPerson();
          }
          muslo1.rotation.x = 0;
          Shoe21.rotation.x = 0;
          person.add(muslo1);
          person.add(Shoe21);
          renderer.render(scene, camera);
          const loader = new THREE.FontLoader();
          loader.load( '/assets/images/fonts/font.json', function ( font ) {
          const geometry = new THREE.TextGeometry("A1", {
            font: font,
            size: 80,
            height: 5,
            curveSegments: 12,
            bevelEnabled: true,
            bevelThickness: 10,
            bevelSize: 8,
            bevelOffset: 0,
            bevelSegments: 5
          } );
            const textMaterial = new THREE.MeshPhongMaterial({ color: 0x000000, });
            const text = new THREE.Mesh(geometry, textMaterial);
            text.material.depthTest = false;
            text.renderOrder = 56;
            scene.add(text);
            renderer.render(scene, camera);
          } );
          return;
        }
        caminar("bottom");
      }, 1500);
    }
  };

  useEffect(() => {
    ref.current?.appendChild(renderer.domElement);
    scene.background = new THREE.Color(0x565656);
    new THREE.TextureLoader().load( "/assets/images/img_tabla-madera-300x300.jpg", (texture) => {
      const table = new THREE.Group();
      const geometry = new THREE.CylinderGeometry(1.3, 1.3, 0.1, 64);
      const material = new THREE.MeshPhongMaterial({ map: texture, opacity: 0.8, polygonOffset: true, polygonOffsetFactor: 0, });
      const cube = new THREE.Mesh(geometry, material);
      cube.renderOrder = 15;
      cube.material.depthTest = false;
      // cube.rotation.x = 35;
      // cube.rotation.y = 50;
      cube.position.y = -2;
      
      table.add(cube);

      const geometryPalo1 = new THREE.CylinderGeometry(0.1, 0.1, 2, 64);
      const materialPalo1 = new THREE.MeshPhongMaterial({ map: texture, opacity: 0.8, polygonOffset: true, polygonOffsetFactor: 0, });
      const palo1 = new THREE.Mesh(geometryPalo1, materialPalo1);
      palo1.renderOrder = 10;
      palo1.material.depthTest = false;
      // palo1.rotation.x = 35;
      // palo1.rotation.y = 50;
      palo1.position.y = -3;
      palo1.position.x = -1;
      
      table.add(palo1);

      const geometrypalo2 = new THREE.CylinderGeometry(0.1, 0.1, 2, 64);
      const materialpalo2 = new THREE.MeshPhongMaterial({ map: texture, opacity: 0.8, polygonOffset: true, polygonOffsetFactor: 0, });
      const palo2 = new THREE.Mesh(geometrypalo2, materialpalo2);
      palo2.renderOrder = 4;
      palo2.material.depthTest = false;
      // palo2.rotation.x = 35;
      // palo2.rotation.y = 50;
      palo2.position.y = -3;
      palo2.position.x = 1;
      
      table.add(palo2);
      table.position.x = -0.5;

      scene.add(table);

      camera.position.z = 5;
      const color = 0xffffff;
      const intensity = 1;
      const light = new THREE.DirectionalLight(color, intensity);
      light.position.set(-1, 2, 4);
      light.castShadow = true;
      scene.add(light);

      renderer.render(scene, camera);
    });
    const pared1Geometry = new THREE.BoxGeometry(1.4, 5, 0.5);
    const pared1Matrial = new THREE.MeshPhongMaterial({ color: 0xEDF3FF, });
    const pared1 = new THREE.Mesh(pared1Geometry, pared1Matrial);

    pared1.position.y = 1;
    pared1.position.x = -1.4;
    pared1.rotation.y = -15;
    pared1.renderOrder = 0;
    pared1.material.depthTest = false;

    scene.add(pared1);

    const pared2Geometry = new THREE.BoxGeometry(1.4, 1.25, 0.5);
    const pared2Matrial = new THREE.MeshPhongMaterial({ color: 0xEDF3FF, });
    const pared2 = new THREE.Mesh(pared2Geometry, pared2Matrial);

    pared2.position.y = -0.6;
    pared2.position.x = 1.4;
    pared2.rotation.y = 15;
    pared2.renderOrder = 0;
    pared2.material.depthTest = false;

    scene.add(pared2);

    const scale = new THREE.Group();

    const escalon1Geometry = new THREE.BoxGeometry(1.3, 0.25, 0.5);
    const escalon1Matrial = new THREE.MeshPhongMaterial({ color: 0x245180, });
    const escalon1 = new THREE.Mesh(escalon1Geometry, escalon1Matrial);

    escalon1.position.y = -0.18;
    escalon1.position.x = 0;
    escalon1.rotation.x = 0.25;
    escalon1.rotation.y = 0;
    escalon1.renderOrder = 0;
    escalon1.material.depthTest = false;

    scale.add(escalon1);

    const escalon2Geometry = new THREE.BoxGeometry(1.5, 0.25, 0.5);
    const escalon2Matrial = new THREE.MeshPhongMaterial({ color: 0x245180, });
    const escalon2 = new THREE.Mesh(escalon2Geometry, escalon2Matrial);

    escalon2.position.y = -0.55;
    escalon2.position.x = 0;
    escalon2.rotation.x = 0.25;
    escalon2.rotation.y = 0;
    escalon2.renderOrder = 0;
    escalon2.material.depthTest = false;

    scale.add(escalon2);

    const escalon3Geometry = new THREE.BoxGeometry(1.7, 0.3, 0.5);
    const escalon3Matrial = new THREE.MeshPhongMaterial({ color: 0x245180, });
    const escalon3 = new THREE.Mesh(escalon3Geometry, escalon3Matrial);

    escalon3.position.y = -0.99;
    escalon3.position.x = 0;
    escalon3.rotation.x = 0.25;
    escalon3.rotation.y = 0;
    escalon3.renderOrder = 0;
    escalon3.material.depthTest = false;

    scale.add(escalon3);

    const escalon4Geometry = new THREE.BoxGeometry(1.9, 0.25, 0.5);
    const escalon4Matrial = new THREE.MeshPhongMaterial({ color: 0x245180, });
    const escalon4 = new THREE.Mesh(escalon4Geometry, escalon4Matrial);

    escalon4.position.y = -1.45;
    escalon4.position.x = 0;
    escalon4.rotation.x = 0.25;
    escalon4.rotation.y = 0;
    escalon4.renderOrder = 0;
    escalon4.material.depthTest = false;

    scale.add(escalon4);

    scene.add(scale);

    const boxClearenceGeometry = new THREE.CylinderGeometry(1, 1, 0.7, 10);
    const boxClearenceMaterial = new THREE.MeshPhongMaterial({ color: 0x313131, });
    const boxClearence = new THREE.Mesh(boxClearenceGeometry, boxClearenceMaterial);

    boxClearence.renderOrder = 10;
    boxClearence.position.y = 2.8;
    boxClearence.rotation.x = 0.6;

    scene.add(boxClearence);

    const boxClearence2Geometry = new THREE.CylinderGeometry(0.8, 0.8, 0.5, 10);
    const boxClearence2Material = new THREE.MeshPhongMaterial({ color: 0x4B4B4B, });
    const boxClearence2 = new THREE.Mesh(boxClearence2Geometry, boxClearence2Material);

    boxClearence2.renderOrder = 10;
    boxClearence2.material.depthTest = false;
    boxClearence2.position.y = 2.8;
    boxClearence2.rotation.x = 0.6;

    scene.add(boxClearence2);

    const geometryPalo1Box = new THREE.CylinderGeometry(0.07, 0.07, 1, 64);
    const materialPalo1Box = new THREE.MeshPhongMaterial({ color: 0x000000, });
    const palo1Box = new THREE.Mesh(geometryPalo1Box, materialPalo1Box);
    palo1Box.renderOrder = 4;
    palo1Box.material.depthTest = false;
    // palo1Box.rotation.x = 35;
    // palo1Box.rotation.y = 50;
    palo1Box.position.y = 3.5;
    palo1Box.position.x = 0.9;
    
    scene.add(palo1Box);

    const geometryPalo2Box = new THREE.CylinderGeometry(0.07, 0.07, 1, 64);
    const materialPalo2Box = new THREE.MeshPhongMaterial({ color: 0x000000, });
    const palo2Box = new THREE.Mesh(geometryPalo2Box, materialPalo2Box);
    palo2Box.renderOrder = 4;
    palo2Box.material.depthTest = false;
    // palo2Box.rotation.x = 35;
    // palo2Box.rotation.y = 50;
    palo2Box.position.y = 3.5;
    palo2Box.position.x = -0.9;
    
    scene.add(palo2Box);

    const floorGeometry = new THREE.BoxGeometry(4, 5, 0.5);
    const floorMatrial = new THREE.MeshBasicMaterial({ color: 0xffffff, });
    const floor = new THREE.Mesh(floorGeometry, floorMatrial);

    floor.position.y = -1.45;
    floor.position.x = 0;
    floor.rotation.x = -1.25;
    floor.renderOrder = -1;
    floor.material.depthTest = false;

    scene.add(floor);

    const personAgent = new THREE.Group();

    // HEAD
    const geometryHead = new THREE.CylinderGeometry(0.4, 0.2, 0.9, 60);
    const materialHead = new THREE.MeshPhongMaterial( { color: 0xFFEACA } );
    const circle1 = new THREE.Mesh( geometryHead, materialHead );

    const geometryCircle2 = new THREE.SphereGeometry( 0.5, 10, 10 );
    const materialCircle2 = new THREE.MeshPhongMaterial( { color: 0xFFEACA } );
    const circle2 = new THREE.Mesh( geometryCircle2, materialCircle2 );

    const geometrycabll = new THREE.SphereGeometry( 0.35, 10, 10 );
    const materialcabll = new THREE.MeshPhongMaterial( { color: 0xffffff } );
    const cabll = new THREE.Mesh( geometrycabll, materialcabll );

    const yee2 = new THREE.CylinderGeometry(0.1, 0.1, 0.1, 45);
    const eye2Material = new THREE.MeshPhongMaterial( { color: 0xCAFFFA } );
    const eye2 = new THREE.Mesh( yee2, eye2Material );

    const yees = new THREE.CylinderGeometry(0.1, 0.1, 0.1, 2);
    const eyeMaterial = new THREE.MeshPhongMaterial( { color: 0xCAFFFA } );
    const eye = new THREE.Mesh( yees, eyeMaterial );

    eye2.position.y = 0.3;
    eye2.position.x = -0.25;
    eye2.renderOrder = 2;
    eye2.material.depthTest = false;
    eye.position.y = 0.35;
    eye.position.x = -0.5;
    eye.material.depthTest = false;
    circle2.position.y = 0.2;
    circle2.renderOrder = 2;
    circle2.material.depthTest = false;
    circle1.renderOrder = 2;
    cabll.position.y = 0.05;
    cabll.position.x = -0.3;
    cabll.renderOrder = 2;

    personAgent.add(circle1);
    personAgent.add(circle2);
    personAgent.add(cabll);
    personAgent.add(eye2);
    personAgent.add(eye);

    //BODY
    
    new THREE.TextureLoader().load("/assets/images/ropa/white.jpg", (texture) => {
      const geometrybody = new THREE.CylinderGeometry(0.4, 0.3, 0.3, 60);
      const materialbody = new THREE.MeshPhongMaterial( { map: texture, } );
      const body = new THREE.Mesh( geometrybody, materialbody );

      body.material.depthTest = false;
      body.renderOrder = 1;
      body.position.y = -0.55;
      body.rotation.x = 0;

      const geometrybody2 = new THREE.CylinderGeometry(0.4, 0.5, 1.2, 60);
      const materialbody2 = new THREE.MeshPhongMaterial( { map: texture, } );
      const body2 = new THREE.Mesh( geometrybody2, materialbody2 );
  
      body2.material.depthTest = false;
      body2.renderOrder = 0;
      body2.position.y = -1.2;
      body2.rotation.x = 0;

      const geometryarm1agent = new THREE.CylinderGeometry(0.2, 0.2, 1.4, 60);
      const materialarm1agent = new THREE.MeshPhongMaterial( { map: texture, } );
      const arm1agent = new THREE.Mesh( geometryarm1agent, materialarm1agent );
  
      arm1agent.material.depthTest = false;
      arm1agent.renderOrder = 2;
      arm1agent.position.y = -1.5;
      arm1agent.rotation.x = 0;

      const geometryhandagent = new THREE.CylinderGeometry(0.1, 0.15, 0.2, 60);
      const materialhandagent = new THREE.MeshPhongMaterial( { color: 0xFFEACA, } );
      const handagent = new THREE.Mesh( geometryhandagent, materialhandagent );
  
      handagent.material.depthTest = false;
      handagent.renderOrder = 1;
      handagent.position.y = -2.35;
      handagent.rotation.x = 0;

      const geometryfingerOne = new THREE.CylinderGeometry(0.03, 0.03, 0.23, 60);
      const materialfingerOne = new THREE.MeshPhongMaterial( { color: 0xFFEACA, } );
      const fingerOne = new THREE.Mesh( geometryfingerOne, materialfingerOne );
  
      fingerOne.material.depthTest = false;
      fingerOne.renderOrder = 0;
      fingerOne.position.y = -2.57;
      fingerOne.rotation.x = 0;
      fingerOne.position.x = -0.05;

      const geometryfingerTwo = new THREE.CylinderGeometry(0.03, 0.03, 0.3, 60);
      const materialfingerTwo = new THREE.MeshPhongMaterial( { color: 0xFFEACA, } );
      const fingerTwo = new THREE.Mesh( geometryfingerTwo, materialfingerTwo );
  
      fingerTwo.material.depthTest = false;
      fingerTwo.renderOrder = 0;
      fingerTwo.position.y = -2.57;
      fingerTwo.position.x = 0.02;

      const geometryfingerThree = new THREE.CylinderGeometry(0.03, 0.03, 0.28, 60);
      const materialfingerThree = new THREE.MeshPhongMaterial( { color: 0xFFEACA, } );
      const fingerThree = new THREE.Mesh( geometryfingerThree, materialfingerThree );
  
      fingerThree.material.depthTest = false;
      fingerThree.renderOrder = 0;
      fingerThree.position.y = -2.57;
      fingerThree.position.x = 0.1;

      const geometryfingerFour = new THREE.CylinderGeometry(0.03, 0.03, 0.28, 60);
      const materialfingerFour = new THREE.MeshPhongMaterial( { color: 0xFFEACA, } );
      const fingerFour = new THREE.Mesh( geometryfingerFour, materialfingerFour );
  
      fingerFour.material.depthTest = false;
      fingerFour.renderOrder = 0;
      fingerFour.position.y = -2.45;
      fingerFour.position.x = -0.12;

      const geometryoreja = new THREE.CylinderGeometry(0.03, 0.03, 0.2, 60);
      const materialoreja = new THREE.MeshPhongMaterial( { color: 0xFFEACA, } );
      const oreja = new THREE.Mesh( geometryoreja, materialoreja );
  
      oreja.material.depthTest = false;
      oreja.renderOrder = 8;
      oreja.position.y = 0.2;
      oreja.position.x = 0.12;

      personAgent.add(body);
      personAgent.add(oreja);
      personAgent.add(arm1agent);
      personAgent.add(handagent);
      personAgent.add(fingerOne);
      personAgent.add(fingerTwo);
      personAgent.add(fingerThree);
      personAgent.add(fingerFour);
      personAgent.add(body2);
      scene.add(personAgent);
      renderer.render(scene, camera);
    });

    const leggeometry = new THREE.BoxGeometry(0.4, 3, 0.4);
    const materialleg = new THREE.MeshPhongMaterial( { color: 0x4B87FF, } );
    const leg = new THREE.Mesh( leggeometry, materialleg );

    leg.material.depthTest = false;
    leg.renderOrder = 0;
    leg.position.y = -3.4;
    leg.position.x = 0;

    personAgent.add(leg);

    personAgent.position.y = -0.8;
    personAgent.position.x = 1.5;

    const hair1Geometry = new THREE.BoxGeometry(0.5, 0.2, 1);
    const materialhair1 = new THREE.MeshPhongMaterial( { color: 0xE0B215, } );
    const hair1 = new THREE.Mesh( hair1Geometry, materialhair1 );

    hair1.material.depthTest = false;
    hair1.renderOrder = 5;
    hair1.position.y = 0.6;
    hair1.position.x = 0;

    personAgent.add(hair1);

    const hair2Geometry = new THREE.BoxGeometry(0.5, 0.3, 0.1);
    const materialhair2 = new THREE.MeshPhongMaterial( { color: 0xE0B215, } );
    const hair2 = new THREE.Mesh( hair2Geometry, materialhair2 );

    hair2.material.depthTest = false;
    hair2.renderOrder = 4;
    hair2.position.y = 0.33;
    hair2.position.x = 0.25;

    personAgent.add(hair1);
    personAgent.add(hair2);

    // const yee2 = new THREE.CylinderGeometry(0.1, 0.1, 0.1, 45);
    // const eye2Material = new THREE.MeshPhongMaterial( { color: 0xCAFFFA } );
    // const eye2 = new THREE.Mesh( yee2, eye2Material );

    scene.add(personAgent);
    renderer.render(scene, camera);
    createPerson(false, 1);
    createPerson(true, 1, 0.05, 1);
    createPerson(true, 1, 0.05, 2);
  });
  return (
    <>
      { children }
      <div ref={ref}></div>
    </>
  )
};
