import React, { useState } from "react";
// import Box from "./Box";

// const DEFAULT_IMAGE = "https://wattention.com/wp-content/uploads/2020/07/winter-001-1.jpg";

export default function Home() {
  const [turn, setTurn] = useState("0");
  const [lastTurn, setLastTurn] = useState("0");

  const timeout = setTimeout(() => {
    setTurn("A1");
    setLastTurn("A2");
    clearTimeout(timeout);
  }, 2000);

  return (
    <article className="Article">
      <p><strong>Turno: { turn }</strong></p>
      <p>
        <span>En espera: { lastTurn }</span>
      </p>
    </article>
  )
}
