import React from "react";
import ReactDOM from "react-dom";
import App from "./components/App";
import Home from "./components/Home";
import "./css/index.scss";

ReactDOM.render(<App>
  <Home/>
</App>, document.querySelector("#app"));
